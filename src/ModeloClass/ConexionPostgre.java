/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloClass;

import gerencialesbeta.RegistroSocios;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author FRANCISCO
 */
public class ConexionPostgre {
    
     public Connection conexion = null;
     String url = "jdbc:postgresql://localhost:5432/Gerenciales";
     String user = "openpg";
     String password = "openpgpwd";
   
    public Connection ConexionBD(){
        
        try {
        Class.forName("org.postgresql.Driver");
        JOptionPane.showMessageDialog(null, "Conectando...");
        }catch(ClassNotFoundException e ){
              e.getMessage();
       }
        
        
        try {
            
            conexion = DriverManager.getConnection(url, user, password);
            JOptionPane.showMessageDialog(null, "Conexion Exitosa!!!");
        } catch (SQLException ex) {
            Logger.getLogger(ConexionPostgre.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Fallo al Conectar");
        }
        
        return conexion;


    }

    
}
