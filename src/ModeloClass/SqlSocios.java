package ModeloClass;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author FRANCISCO
 */
public class SqlSocios extends ConexionPostgre{
    
    public boolean Registrar(Socios scs )
    {
   
        try {
            String SQLQuery = " INSERT INTO public.\"Socios\"(\n" +
"	idsocios, \"Nombres\", \"Apellidos\", \"Telefono\", \"Empresa\", \"Paquete\", \"Contraseña\")\n" +
"	VALUES (?, ?, ?, ?, ?, ?, ?); ";
            Connection conexion = ConexionBD();
            PreparedStatement Pst = conexion.prepareStatement(SQLQuery);
           
            Pst.setInt(1, scs.getIdsocios());
            Pst.setString(2, scs.getNombres());
            Pst.setString(3, scs.getApellidos());
            Pst.setString(4, scs.getTelefono());
            Pst.setString(5, scs.getEmpresa());
            Pst.setString(6, String.valueOf(scs.getPaquete()));
            Pst.setString(7, scs.getContraseña());
           
            Pst.executeUpdate();
           // JOptionPane.showMessageDialog(null, "Registro Exitoso");
            return true;
            
            
        } catch (SQLException sQLException) {
            JOptionPane.showMessageDialog(null, sQLException);
           // JOptionPane.showMessageDialog(null, "No se Pudo Registar");
        }
        return false;
        
        
    }
    
    public boolean Login(Socios scs )
    {
   
        try {
           // String SQLQuery = " SELECT idsocios, Nombres, Apellido, Paquete, Contraseña FROM public.Socios WHERE public.Socios = ? ";
            String SQLQuery = " SELECT u.\"idsocios\", u.\"Nombres\", u.\"Apellidos\", u.\"Paquete\", u.\"Contraseña\", t.\"Paquete\"\n" + "FROM public.\"Socios\" AS u INNER JOIN public.\"Paquetes\" AS t ON u.\"Paquete\"=t.\"Paquete\"  WHERE \"Nombres\"= ? LIMIT 1 ; ";
            Connection conexion = ConexionBD();
            PreparedStatement Pst = conexion.prepareStatement(SQLQuery);
            ResultSet Rs = null;
           
            Pst.setString(1, scs.getNombres());
            Rs = Pst.executeQuery();
           // JOptionPane.showMessageDialog(null, "Registro Exitoso");
           if(Rs.next())
           {
                
               if(scs.getContraseña().equals(Rs.getString(5)))
               {
                   
             String SQLUpdate = " UPDATE public.\"Socios\"\n" + "SET  \"Ulsesion\"=?\n" + "WHERE idsocios =?; ";
               Pst = conexion.prepareStatement(SQLUpdate);
               Pst.setString(1, scs.getUlsesion());
               Pst.setInt(2, Rs.getInt(1));
               Pst.execute();
               
               scs.setIdsocios(Rs.getInt(1));
               scs.setApellidos(Rs.getString(3));
               scs.setPaquete(Rs.getString(4));
               scs.setPaqueteT(Rs.getString(6));
               
               return true;
               }
               else
               {
               JOptionPane.showMessageDialog(null, "Contraseña Incorrecta");
               return false;
               }
           }
            
            return false;
            
        } catch (SQLException sQLException) {
            JOptionPane.showMessageDialog(null, sQLException);
           // JOptionPane.showMessageDialog(null, "No se Pudo Registar");
        }
        return false;
        
    

    }
}
