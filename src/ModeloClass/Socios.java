package ModeloClass;


/**
 *
 * @author FRANCISCO
 */
public class Socios {
    
    private int idsocios;
    private String Nombres;
    private String Apellidos;
    private String Telefono;
    private String Paquete;
    private String Empresa;
    private String Contraseña;
    private String Ulsesion;
    private String PaqueteT;

    public String getUlsesion() {
        return Ulsesion;
    }

    public void setUlsesion(String Ulsesion) {
        this.Ulsesion = Ulsesion;
    }

    public int getIdsocios() {
        return idsocios;
    }

    public void setIdsocios(int idsocios) {
        this.idsocios = idsocios;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public String getPaquete() {
        return Paquete;
    }

    public void setPaquete(String Paquete) {
        this.Paquete = Paquete;
    }

    public String getEmpresa() {
        return Empresa;
    }

    public void setEmpresa(String Empresa) {
        this.Empresa = Empresa;
    }

    public String getContraseña() {
        return Contraseña;
    }

    public void setContraseña(String Contraseña) {
        this.Contraseña = Contraseña;
    }

    public String getPaqueteT() {
        return PaqueteT;
    }

    public void setPaqueteT(String PaqueteT) {
        this.PaqueteT = PaqueteT;
    }
    
    
    
}
